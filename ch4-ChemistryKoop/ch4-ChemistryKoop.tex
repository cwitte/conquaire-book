\chapter{Reproducing experiments in ice nucleation in atmospheric chemistry}

\label{g3Koop}


\chapterauthor[1]{Vidya Ayer}
\chapterauthor[1]{Philipp Cimiano}
\chapterauthor[2]{Fabian Hermann}
\chapterauthor[2]{Evelyn Jantsch}
\chapterauthor[2]{Thomas Koop}
\begin{affils}
     \chapteraffil[1]{Faculty of Technology, Bielefeld University}
     \chapteraffil[2]{Faculty of Chemistry, Bielefeld University}
\end{affils} 


Vidya Ayer

Philipp Cimiano

Fabian Hermann

Evelyn Jantsch

Thomas Koop

Faculty of Technology, Bielefeld University

Faculty of Chemistry, Bielefeld University


%S-1
\section{Abstract} \label{abstract}
%S-1

This chapter describes a case study in reproducing results in the are of atmospheric chemistry. The specific result reproduced is described in the paper \emph{`BINARY: an optical freezing array for assessing temperature and time dependence of heterogeneous ice nucleation'} by Budke and Koop \cite{Budke2015}.
The study investigated the conditions under which ice nucleation occurs using Snomax\textsuperscript{\textregistered}, a commercial ice inducer containing freeze-dried nonviable bacterial cells from Pseudomonas syringae, as a test substance for the investigation of heterogeneous ice nucleation processes. The ice inducing bacterial cell agents are known to be active at high temperature and are used in snow cannons. The study investigated the ratio of nucleations
in multi-group intercomparison setting, considering a temperature range between 0~°C and -12~°C. The main result was the finding that two classes of nucleations occur at a number ratio of about 1 to 1000 in the chemical samples, based on the difference in 3 orders of magnitude of the temperature plateau values.

\subsection*{Keywords} \label{keywords}
Atmospheric Chemistry, Ice Nucleation

%S-2
\section{Introduction} \label{intro}
%S-2

The study of ice formation is an active research area  in the atmospheric sciences \cite{Pruppacher1997}. For example, ice crystals occur in high altitude clouds and they are responsible for initiating most precipitation above continents \cite{Pruppacher1997,Cantrell2005a,Lamb2011}. From a thermodynamic point of view, crystalline ice is the stable phase of water below the melting temperature $T_m$, which is 0~°C at ambient pressure, see Figure \ref{supercooled_liquid}.
In many cases, the formation of ice crystals is kinetically inhibited and can occur at lower temperature either via homogeneous or via heterogeneous nucleation, see Figure \ref{supercooled_liquid}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=10cm]{./images/fig1-supercooled_liquid.pdf}
	\caption{Schematic picture of different nucleation mechanisms for the freezing of water. $T_m$ is the melting temperature of the crystalline phase ice. Adapted with changes from Koop,~2004 \cite{Koop2004b}.}
	\label{supercooled_liquid}
\end{figure}


For homogeneous freezing a number of water molecules have to arrange themselves into an ice-like cluster, termed critical ice embryo, in order to trigger ice formation. The size of this critical embryo is temperature dependent and decreases with decreasing temperature thus making ice nucleation more likely at lower temperature. For example, micrometer-sized pure water droplets freeze homogeneously at approximately -38~°C (homogeneous nucleation temperature) \cite{Koop2004b}. In contrast, heterogeneous ice nucleation can occur at higher temperatures – even close to the melting temperature of ice – depending upon the presence and activity of so-called ice nuclei (IN) , see Figure \ref{supercooled_liquid} \cite{Koop2004b,Cantrell2005a,DeMott2010,Murray2012b}. Laboratory experiments can be employed to help understanding the processes that lead to ice nucleation in the atmosphere. By investigating ice nucleation temperatures of different IN, we can quantify different IN activities, which can be used for parametrizations of ice formation in atmospheric cloud models \cite{Hoose2012}. 

The activity of an IN material suspended in a water droplet can be obtained from the measured number of active sites per dry mass $n_m(T)$ as a function of temperature. Equation \ref{def_n_m} presents a definition for $n_m(T)$, where $T$ is temperature, $K(T)$ is the experimentally observed cumulative number of active sites per volume of water, and $C_m$ is the mass concentration of IN in the water.

\begin{equation}
n_m(T)=\frac{K(T)}{C_m} 
\label{def_n_m}
\end{equation}

$K(T)$ can be obtained from equation \ref{def_f_ice}.

\begin{equation}
f_{ice}(T)=\frac{n_{ice}(T)}{n_{tot}}=1-e^{-K(T) \cdot V_{drop}}
\label{def_f_ice}
\end{equation}

Here, $f_{ice}(T)$ represents the experimentally observed cumulative ice fraction, which is defined by the ratio of the number of droplets frozen at temperature $T$, $n_{ice}(T)$, and the total number of investigated droplets, $n_{tot}$. $V_{drop}$ is the droplet volume.

Established methods for the determination of heterogeneous ice nucleation temperatures have different advantages and disadvantages. For instance, larger droplet volumes encounter a higher probability of impurities. In contrast, smaller volumes are often realized through emulsions and, therefore, an oil phase is in contact with the water droplet, which may influence results for those IN (e.g. pollen and fungal spores), which have an affinity to hydrophobic phases, i.e. the concentration of suspended IN would be overestimated in such cases \cite{Pummer2012}. Many experimental techniques are droplet arrays based on a method originally developed by Vali and Stansbury, where small volume droplets are placed on a cooling stage \cite{Vali1966,Vali1971}. However, since no oil phase is used to enclose the droplets, frozen droplets grow by water vapor transport from the remaining supercooled liquid droplets, according to the Wegener-Bergeron-Findeisen process. Moreover, sometimes frost halos form around frozen droplets. These ice rings tend to grow on the surface below the droplets and can cause ice nucleation in adjacent supercooled droplets. Budke and Koop \cite{Budke2015} took these potential shortcomings into account when they developed a new device to investigate ice nucleation termed BINARY (short for Bielefeld Ice Nucleation ARraY), which was used in the present study, see below. The different droplets in BINARY are separated in individual compartments thus preventing water vapor transfer between neighboring droplets. 

Snomax\textsuperscript{\textregistered} is a well-studied IN material and, therefore, a good reference substance for testing new methods \cite{Maki1974,Vali1976,Mohler2008}. Snomax\textsuperscript{\textregistered} is a commercial product containing freeze-dried cells from \textit{Pseudomonas syringae}, a rod-shaped bacterium living on a variety of plants. \textit{P. syringae} bacteria are known to induce heterogeneous ice nucleation at very high temperatures of approximately -2~°C (class A) and also in a temperature range of about -7 to -10~°C (class C) \cite{Wex2015}. The latter study was a multi-group intercomparison project and also included data from the BINARY setup. Using this setup Budke and Koop determined $n_m(T)$ in a temperature range between 0~°C and -12~°C \cite{Budke2015}.


%S-3
\section{Methods} \label{methods}
%S-3
In this section we describe the experimental settings and methods as well as the main results described in the paper by Budke and Koop \cite{Budke2015}. 


%SS-3.1
\subsection{Experiment settings and Data acquisition pipeline} \label{XperimentData}
%SS-3.1


In the study under investigation \cite{Budke2015}, the BINARY technique was used to determine heterogeneous ice nucleation temperatures of Snomax\textsuperscript{\textregistered}. Therefore a certain dry mass of Snomax\textsuperscript{\textregistered} ($m$) was suspended in freshly double-distilled water of volume $V_{H_2O}$ to obtain the desired mass concentration $C_m=m/V_{H_20}$  of Snomax\textsuperscript{\textregistered} in water. 36 droplets (each $V_{drop}$ = 1~µL) of such a suspension were pipetted into the compartments of a polydimethylsiloxane (PDMS) lattice placed on a hydrophobic glass surface, resulting in a 6 x 6 droplet array as shown in Figure \ref{binary_setup}a. The droplet compartments are sealed with another glass slide on top of the PDMS lattice to prevent droplet evaporation (see Figure \ref{binary_setup}b).

\begin{figure}[htb]
	\centering
	\includegraphics[width=10cm]{./images/fig2-BINARY_setup.pdf}
	\caption{Schematic picture of the Bielefeld Ice Nucleation ARraY (BINARY) setup. \textbf{(a)} Top view of the 6 x 6 droplet array. The droplets are separated from each other by a polymer lattice creating individual compartments. \textbf{(b)} Side view showing the sealing of the compartments by top and bottom glass slides. \textbf{(c)} Position of the sample array on the Peltier cooling stage inside the cooling chamber. Figure is taken from Budke and Koop,~2015 \cite{Budke2015}.}
	\label{binary_setup}
\end{figure}

This sample array is positioned on a Peltier stage within a cooling chamber (Linkam LTS120) as shown in Figure \ref{binary_setup}c. A metal frame presses the whole array onto the Peltier cooling stage with the help of fixing screws to assure a homogeneous and efficient heat transfer. The Peltier stage is connected to a heat sink bath at 5~°C and its top side can be cooled to -40~°C at cooling rates between 0.1 and 10~°C  min$^{-1}$. All experiments described below were measured at a cooling rate of 1~°C  min$^{-1}$. Small cold-light white LED stripes are fixed at the top edges inside the cooling chamber aiding the visualization of phase changes through light scattering (liquid droplets appear darker whereas ice crystals appear brighter due to the backscattered light). A CCD camera (QImaging MicroPublisher 5.0 RTV) is mounted above the whole setup to observe the droplets through a 40 x 40 mm window in the top ceiling of the cooling chamber. Both, the interior of the cooling chamber and the surface of the top window are purged with dry nitrogen during the experiment to prevent water condensation. A LabVIEW{\texttrademark} virtual instrument is used to detect ice nucleation and melting events from the digital images obtained by the CCD camera. In detail, for each compartment the average gray value of all pixels within a predefined area is obtained. These gray values range between 0 for black pixels and 255 for white pixels.

Figure \ref{binary_example}b and c show a representative behavior of the gray values and their changes for the compartment marked by a yellow box in panel (a). Starting with the red curve at 4~°C, the average gray value in Figure \ref{binary_example}b is almost constant until the droplet freezes at -3.9~°C, as indicated by a sharp jump. This steep increase is also shown as the derivative in Figure \ref{binary_example}c. After a temperature of -10~°C is reached, heating is started (green curves) and ice melting begins at 0~°C, again indicated by a gray value change. The thresholds for defining the occurrence of nucleation and melting events are gray value changes larger than 1 and -1, respectively (dotted red and green lines in Figure \ref{binary_example}c).

\begin{figure}[htp]
	\centering
	\includegraphics[width=8cm]{./images/fig3-BINARY_example.pdf}
	\caption{Typical experiment with Snomax\textsuperscript{\textregistered}-containing droplets (0.1~µg~µL$^{-1}$) showing the automatic detection of ice nucleation events by the change in brightness during freezing. \textbf{(a)} Image series of the 6 x 6 droplet array during cooling. \textbf{(b)} Measured gray value of the droplet compartment indicated by the yellow box in panel (a) during cooling (red) and heating (green). Freezing and melting start at -3.9~°C and 0.0~°C, respectively. \textbf{(c)} Plot of the change in gray value between successive images showing peaks at the phase transition temperatures. Threshold values of ±1 for the automatic attribution of freezing and melting are indicated by the dashed line. Figure is taken from Budke and Koop,~2015 \cite{Budke2015}.}
	\label{binary_example}
\end{figure}



%SS-3.2
\subsection{Methods applied to analyze the experiment data} \label{MethodsXpDat}
%SS-3.2


For each individual droplet, the uncalibrated heterogeneous ice nucleation temperatures $T_{nuc}$ is obtained and saved in a text file for offline calibration and further analysis. The temperature calibration function and how it was developed from experiments is discussed in detail in the paper \cite{Budke2015}. Briefly, the calibrated nucleation temperature $T_{cal}$ can be obtained using equation \ref{T_cal}, where $r$ is the cooling rate of 1~°C min$^{-1}$.

\begin{multline}
T_{cal}=-((-6.03165)+0.02113\cdot (273.15+T_{nuc})-(3.59774+(-0.02956) \\ (273.15+T_{nuc})+6.10156 \cdot 10^{-5} \cdot (273.15+T_{nuc})^2 ) \cdot (-r) +T_{nuc}
\label{T_cal}
\end{multline}

Each $T_{cal}$ value is then binned into temperature intervals of 0.1~°C width, i.e. all $T_{cal}$ values within the interval $X_{low} \leq T_{cal} < X_{up}$ get sorted into the bin $X_{low}$. Thereafter, $T_{cal}$ will only be used as the binned value $T$. Now the number of individual $T_{cal}$ data are counted to gain $n_{ice}(T)$ and $n_{tot}$ for determining $f_{ice}$ using equation \ref{def_f_ice}. This counting is done for all droplets with the same Snomax\textsuperscript{\textregistered} concentration, so each measured concentration has one cumulative ice fraction ranging from 0 to 1. Using equation \ref{n_m} (derived from equation \ref{def_n_m} and \ref{def_f_ice}) $n_m(T)$ is obtained for each concentration and can be plotted for all investigated temperatures.

\begin{equation}
n_m(T)=\frac{-\ln(1-f_{ice}(T))}{C_m \cdot V_{drop}}
\label{n_m}
\end{equation}

%SS-3.3
\subsection{Main Results} \label{mainresults}
%SS-3.3

Figure \ref{binary_plot} presents the main result of the paper in form of a combined curve of $n_m(T)$ values from several Snomax\textsuperscript{\textregistered} suspensions with different concentrations (see color code). 

\begin{figure}[htbp]
	\centering
	\includegraphics[width=10cm]{./images/fig4-BINARY_Plot.pdf}
	\caption{ Experimentally determined active site density per unit mass of Snomax\textsuperscript{\textregistered} $n_m(T)$ versus temperature. Symbol colors indicate data from droplets with different Snomax\textsuperscript{\textregistered} concentrations; symbol size indicates the number of nucleating droplets per temperature interval (0.1~°C). The temperature range for different classes of IN also indicated by the colored bars. Figure is taken from Budke and Koop,~2015 \cite{Budke2015}.}
	\label{binary_plot}
\end{figure}

Two steep increases can be seen, which represent two different types of IN active at different temperature regimes. Plateaus in a $n_m(T)$ plot, e.g. between -4.5~°C and -7.5~°C,  can be interpreted as temperatures where no IN is active. It should be noted that data points below -12~°C down to \mbox{-35~°C} were obtained, but are not shown since they did not reveal any other IN (purple symbols). Also indicated in Figure \ref{binary_plot} are the temperature ranges for different IN classes as defined in the literature \cite{Turner1990}. Two different classes of IN in Snomax\textsuperscript{\textregistered} were identified, inducing ice nucleation at about -3.5~°C (class A) and at about -8.5~°C (class C). For class A IN in Snomax\textsuperscript{\textregistered} $n_m(T)$ ranges from about 10$^{-2}$~µg$^{-1}$ up to almost 10$^{3}$~µg$^{-1}$. However, the number of active sites is much larger for class C IN as the increase starts at 10$^{3}$~µg$^{-1}$ rising to almost 10$^{6}$~µg$^{-1}$, indicating that class C IN are about a factor of 10$^{3}$ more abundant than class A IN. The number of active sites can also be expressed as a number of active sites per cell (i.e., $n_n(T)$ on the right axis in Figure \ref{binary_plot}). Hence, there is about one class C active site per \textit{P. syringae} cell.



%S-4
\section{Computational Reproducibility} \label{ReX}
%S-4

As a main objective of this study, we defined the goal of being able to independently reproduce the plot reproduced here in Figure \ref{binary_plot} as main result of the work described by Budke and Koop \cite{Budke2015}. The validation was done by calculating the calibrated temperatures from the temperature for a given cooling rate and Snomax concentration and for each concentration bin, the $f_{ice}$ was calculated. The calculations had been done originally using OriginPro for the original paper, while we reproduced these calculations using a Python program.


%SS-4.1
\subsection{Research Data - Primary} \label{RDprimary}
%SS-4.1
Their research data was read off the BINARY experiment setup, then processed entirely by OriginPro, a proprietary computer software from OriginLab Corporation, that is mainly used for interactive scientific graphing and data analysis on the Microsoft Windows platform only. It is a GUI software with a spreadsheet-like front end which uses a column-oriented data processing approach for calculations. It has its own file format, \textbf{.OPJ}, for project files which are directly processed by the system for statistics, data analysis and visualisation. 

The group uses OriginPro along with a scripting language known as \textbf{LabTalk} that allows finer contro, by writing small macros that run over the data analysis process for the experiment data. With LabTalk they program routine operations, including batch operations, with customizable graph templates and analysis dialog box themes. Various features exist to save a collection of operations within the workbook, viz., saving a suite of operations, auto recalculation on changes to data or analysis parameters, and different analysis templates. 


%sss-4.2
\subsection{Research Data - Analyzed and Processed} \label{RD2anlyz}
%sss-4.2

The Snomax\textsuperscript{\textregistered} data file contains data from the OPJ data file that is read into the Origin software system. The data was exported into tab-separated files within Origin as *.txt file with six TAB delimited columns. The calibration data numbers start from line four with the headers confined to the first three lined; viz. the first line has the column names, while line 2 contains the data description or unit, and if available, the third line contained information about the substance.

For the computational reproducibility experiment, we used Python to process these text files for data analysis and visualisation based on the validated raw data. After calibrating the temperature, the python script binned the data, then grouped the data for all columns by concentration (decreasing) into different bins and then within each concentration bin the data is sorted by (decreasing) calibrated temperature $T_{cal}$. Afterwards, $f_{ice}(T)$ was determined for each temperature value in each bin. In the last step the mass concentration of Snomax\textsuperscript{\textregistered} and the volume of the droplets are converted into the active site density per unit mass, $n_{m}(T)$.

After tabulating $f_{ice}(T)$ and $n_{m}(T)$ for each concentration bin, the resulting data is stored in a csv file that became the input data to reproduce the plot from the original paper shown in \ref{binary_plot}. 

%SS-4.2
\subsection{Data Workflow Lifecycle} \label{datLC}
%SS-4.2

In order to reproduce the mentioned plot, the functionality implemented originally in the OriginPro frameworks was reproduced using a Python program.
The resulting workflow implemented in Python is equivalent to the workflow implemented in OriginPro and schematically represented in Figure \ref{f5-data-workflow}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=9cm,height=15cm,keepaspectratio]{./images/fig5-data-workflow.pdf}
    \caption{Schematic representation of analytical workflows implemented in Python program}
    \label{f5-data-workflow}
    \protect
\end{figure}


%%% DESCRIPTION OF PIPELINE %%%
First, the given raw data is read in and each column is saved as a list. In the second step, the calibrated temperature $T_{cal}$ is calculated from the measured temperature $T_{nuc}$ and the cooling rate $r$ with formula \ref{T_cal}.

%\begin{align*}
%T_{cal} &= - [(-6.03165) + 0.02113 \cdot (273.15 + T_{nuk})] \\
% & -[3.59774 + (-0.02956) \cdot (273.15 + T_{nuk}) + 6.10156 \cdot 10^{-5} \\
% & \cdot (273.15 + T_{nuk})^{2}] \cdot (-r) + T_{nuc}
%\end{align*}
%
%After the calculation of the calibrated temperature $T_{cal}$ each value is binned into classes with one decimal, i.e., all values within the interval $T_{low} \leq T_{cal} < T_{up}$ get binned to the value $T_{low}$. After this step $T_{cal}$ is used as the binned value.

In the third step, the data is grouped by the concentration $C_m$ into different bins and is sorted in descending order. Within each bin the data is sorted by the temperature $T$ in descending order.

In the fourth step, for each bin a new table is generated. The bin is grouped by the temperature $T$ and a second row is invented which shows the number of occurrences of each different binned temperature. A third row is used to summarize the occurrences including the current temperature. It shows the number of droplets until the current temperature. This value and the total number of all droplets in this bin are used to calculate the frozen fraction $f_{ice}(T)$ with the given formula \ref{def_f_ice}. Then it is appended to the table.

In the fifth step, the active site density per unit mass, $n_{m}(T)$ is calculated from $f_{ice}(T)$, the concentration $c$ and the droplets volume $V_{drop}$ with the formula \ref{n_m} and is appended as fifth column to the new table. 

Thereafter, this table is saved as a \textbf{.CSV} file, a common data format used by researchers with many tools for file input-output operations. As a second result, the generated table is used to reproduce and plot the graph in Figure \ref{fig6-cqr-sonomaxvstemp} which displays our graph and the graph from the original paper for comparison.

With the given raw data the results from the original experiment could be successfully reproduced using Python, an open source programming language.
%%% END OF DESCRIPTION %%%


%SS-5.3
\subsection{Technical Challenges} \label{techBugs}
%ss-5.3
Due to the complexity and time associated with learning to use a new system like Origin, we opted to use Python to code the formulae and run the data files to be analyzed. 
In addition Python is open source and is supported by many platforms.

%sss-5.3.1
\subsubsection{Software toolkit and File IO standards} \label{SWfileIO}
%sss-5.3.1
Origin software is a proprietary analysis toolbox with no equivalent libre software alternative. Hence, their OPJ data file format can only read data into the Origin software system. The system allows data to be exported into tab-separated files with delimited columns. We depend on the researcher providing these exported data files for our processing pipeline. 

%In the text files, the calibration data numbers start from line four with the headers confined to the first three lined; viz. the first line has the column names, while line 2 contains the data description or unit, and if available, the third line contained information about the substance.



\subsection{Reproducibility Status} \label{ReXStatus}

We reproduced the results of their experiments as shown in the visualisation graph Figure \ref{fig6-cqr-sonomaxvstemp} by plotting $n_{m}(T_{cal})$, the cumulative number of ice nucleators per $\mu \text{g}$ of Snomax\textsuperscript{\textregistered} as a function of calibrated temperature.

For singular deterministic data analysis, the temperature dependence of ice nucleation induced by singular ice nucleators can be described in terms of $n_{m}(T)$, the cumulative number of ice nucleators per mass of Snomax\textsuperscript{\textregistered} as a function of temperature; which can be obtained from droplet freezing array data by analyzing the frozen fraction $f_{ice}(T_{cal})$ from the cumulative number of frozen droplets $n_{ice}(T_{cal})$ and the total number of droplets $n_{tot}$ of a particular Snomax\textsuperscript{\textregistered} concentration. The important formula to calculate $f_{ice}$ is: 
\begin{align*}
f_{ice}(T) &= \frac{n_{ice}(T)}{n_{tot}}
\end{align*}

The active site density per unit mass, $n_{m}(T)$ can be calculated from $f_{ice}(T)$, the concentration $c$ and the droplets volume $V_{drop}$ with the formula:
\begin{align*}
n_{m}(T) = \frac{- ln(1 - f_{ice}(T))}{c \cdot V_{drop}}
\end{align*}

The active site density $n_m(T)$ is plotted versus $T$ for all concentrations with color indications for the concentration and the thickness indications for the number of events for a temperature event as in the paper.



\begin{figure}[!ht]
    \centering
    \includegraphics[width=15cm,keepaspectratio]{./images/fig6-KoopConquaire-snomaxVsTemp.pdf}
    \caption{Experimentally determined active site density per unit mass of
Snomax® nm(T) versus temperature. A: Original version of diagram as published by Budke et al. \cite{Budke2015}; B: diagram resulting from reproducing the computational workflows of Budke et al. as described in this paper.
Symbol colors indicate data from droplets with different Snomax® concentrations; symbol size indicates the
number of nucleating droplets per temperature interval (0.1 °C). The temperature range for different classes of IN also indicated by the colored bars.}
    \label{fig6-cqr-sonomaxvstemp}
    \protect
\end{figure}
%\clearpage


Two particularly strong increases in $n_{m}(T)$, are observed, one at about $\SI{-3.5}{\celsius} (269.6 K) \pm 0.5 K$ and one at $\SI{-8.5}{\celsius} (264.6 K) \pm 0.5 K$, indicating the presence of two distinct classes of ice nucleators with different activation temperatures. 

The two plateaus at temperatures just below each increase of $n_{m}(T)$, in Figure \ref{fig6-cqr-sonomaxvstemp} arise when no ice nucleations active at these temperatures are present in the investigated suspensions. The $n_{m}(T)$, values of the plateaus differ by about 3 orders of magnitude, from which it can be infered that the two classes of Snomax\textsuperscript{\textregistered} ice nulceations occur at a number ratio of about 1 to 1000 in the samples. The active site densities per cell $n_{m}(T_{cal})$, shown in Figure \ref{fig6-cqr-sonomaxvstemp} on the right axis were calculated using the specific particle number of cells in Snomax\textsuperscript{\textregistered}.
    i
%S-5

\section{Summary of reproducibility experiment}

In this work we could successfully reproduce the main results of the paper by Budke et al. \cite{Budke2015} by reproducing the original analytical workflow using OriginPro by using free and open software, in this case a Python program implemented as part of the Conquaire project. In this case we have a case of limited reproducibility as the direct reproduction would have required obtaining a commercial license for OriginPro and re-creating the GUI interactions used in the original work. Instead, we have opted for a re-implementation of the original analysis in Python under guidance of the authors of the original paper. We have thus not directly reproduce the original workflow, but developed a workflow that can be regarded as functionally equivalent. As we did not reproduce the original workflow exactly, we have a case of limited analytical reproducibility as defined in chapter \ref{introduction} of this book. 
The data has been uploaded to the DFG FOR1525 project website (https://www.ice-nuclei.de/), where it is available upon request. Moreover, the data has been verified by an intercomparison paper by Wex et al. \cite{Wex2015}. As a result of Conquaire, both the data and the script are available in a Git repository for further reuse and verification. While there is not yet a DOI for the dataset, the dataset and script are referenceable via a GIT repository, even down to a particular version.


%\section{Summary of findings from computational reproducibility} \label{RevRec}
%%S-5
%Libre software aka., Free and Open Source Software (FOSS) builds and expands the data usage and access for researchers interested in computational reproducibility. Software is also a form of data whose importance is reduced in comparision with experimental research data.  
%
%% ss-5.1
%\subsection{Recommendations for Research Data FAIR-ness} \label{FAIRdat}
%% ss-5.1
%
%FAIR data principles are a set of community built standards that require research data to be \textbf{Findable}, \textbf{Archivable}, \textbf{Interoperable} and \textbf{Reusable} artefacts and further to the technical challenges and issues described in section \ref{ReX}, we reviewed the research data workflow on these facets.
%
%%SS-5.1.1
%\subsubsection{Data should be Findable} \label{fairFind}
%%SS-5.1.1
%
%The data has been uploaded to the DFG FOR1525 project website (https://www.ice-nuclei.de/), where it is available upon request. Moreover, the data were verified by an intercomparison paper by Wex et al. \cite{Wex2015}.
%
%
%The data has not yet been released on PUB or on gitlab, our university repository. It is not directly referancable, that is there is no persistent ID to refer to the dataset.
%
%
%%sss-5.1.2
%\subsubsection{Data should be Accessible} \label{fairAccess}
%%sss-5.1.2
%Licensing software clears the ambiguity around data sharing and reuse, irrespective of whether the code is publicly or privately released.
%To protect the intellectual property (IP) of their work, the code must be released under a Free software license.
%
%%sss-5.1.3
%\subsubsection{Data should be Interoperable} \label{fairInter}
%%sss-5.1.3
%Origin is a proprietary software which makes the data interoperability a difficult proposition. There is no Linux version of the same available so the research group must evaluate libre software options. A solution would be to use open toolkits to ensure software interoperability by building on the existing code to do all their data analysis.
%
%%sss-5.1.4
%\subsubsection{Data should be Reusable} \label{fairReuse}
%%sss-5.1.4
%Data reuse is an expensive option due to the existence of paid software in the researchers workflow, greatly limiting non-domain users interested in reproducible software. The researchers can compartmentalize the tasks of data acquisition, data processing management, data analysis and visualisation while extending the Python code used in this reproducibility. It would ensure a higher rate of data reuse.


%S-6
\section{Conclusion} \label{Concl}
%S-6
This paper has described a case study in computational reproducibility of results in the area of physical chemistry. In particular, we have
aimed at reproducing the analytical workflow that lead to the results published in the paper \emph{`BINARY: an optical freezing array for assessing temperature and time dependence of heterogeneous ice nucleation'} by Budke and Koop \cite{Budke2015}. The central diagram of this work showing the relation between ratio of nucleation in dependence of temperature could be successfully reproduced by reimplementing the original analytical workflow in OriginPro via a Python script. As we did not exactly reproduce the original workflow, we have thus a case of limited analytical reproducibility. As a result of the project, both the derived data and the Python script described in this chapter are available for further re-use and validation of the original results.


%S-7
\section*{Acknowledgments} \label{Ack}
%S-7
For their contributions to this chapter, we thank:

\begin{itemize}
    \item \textbf{Carsten Budke} for the data and technical discussions during the computational reproducibility process. 
\end{itemize}

\bibliographystyle{plain}
\bibliography{ch4-ChemistryKoop}



