%\clearpage
\subsection{The ETACE Research Agenda}

The research group on Economic Theory And Computational Economics (ETACE) is mainly concerned with the analysis of different aspects of economic dynamics and strategic interaction. It employs and extends both analytical methods, in particular dynamic optimization and dynamic game theory, and computational approaches, where the latter includes numerical methods for the solution of (dynamic) equilibrium models as well as agent-based simulations.

Research at ETACE is based on the conviction that a thorough examination of (dynamic) economic phenomena should be based on a combination of (i) dynamic equilibrium analysis, providing benchmark results under full rationality (and foresight) of decision makers, and (ii) the explicit consideration of the economic dynamics unfolding under the interaction of boundedly rational heterogeneous agents. The aim of our work is to extend the toolbox of economists and to apply these tools to relevant research questions, mainly in the areas of Industrial Economics, Labour Economics and Macroeconomic Dynamics.

Ongoing research at ETACE can be broadly categorized in the following research topics:

\begin{itemize}
\item Agent-based Modelling for Economic Policy Analysis
\item Economics of Innovation and Industrial Dynamics
\item Network Formation and Spatial Dynamics
\item Labour Economics and Search Theory
\end{itemize}

\subsection{Link to the Conquaire project}

To link the research carried out within the ETACE group to the Conquaire project, we have selected the first research topic ("Agent-based Modelling for Economic Policy Analysis") as the partner project for Conquaire. More specifically, we have identified the Eurace@Unibi Model, a specific agent-based simulation model, as a case study to test the notions of "analytical reproducibility" and "continuous integration of research data", which are two key aspects of data management for the Conquaire project. In the next subsection we describe the Eurace@Unibi Model in more detail.

\subsection{Agent-based Modelling for Economic Policy Analysis}

In recent years it has been widely acknowledged by economic scholars that the explanatory power of standard representative agent models is in many cases limited. This has lead to a surging interest in the empirical exploration of bounded rationality in economic decision making, mainly by means of laboratory experiments and attempts to incorporate heterogeneity in endowments or behavior into economic models. A particularly natural and promising approach to account for economic phenomena that result from the (bounded) rational interaction of heterogeneous economic agents is the use of agent-based computer simulation models. Phenomena of such type are abundant (the avalanche-like dynamics in the network of connected commercial banks inducing the current economic crisis is just one prominent example in that respect), and a large amount of insightful agent-based research has addressed a wide range of relevant economic issues (see e.g. the Handbook of Computational Economics Volume II edited by Tesfatsion and Judd ((\citealp{ACE_2006})) for an overview, and the more recent Handbook of Computational Economics Volume IV edited by Hommes and LeBaron (\citealp{ACE_2018}) for applications).

\subsubsection{The Eurace@Unibi Macroeconomic Model}

A main research topic at ETACE is the development of micro-founded macro\-economic heterogeneous agent-based models that can be used as an integrated framework for policy analysis in different economic policy areas. Based on work carried out in the European Eurace Project, the Eurace@Unibi model has been developed and used as a tool for the analysis of various economic policy questions related to issues of technological change and economic growth, labor market policies, social cohesion and convergence, and to study banking and credit market regulations. The Eurace@Unibi model is among the most sophisticated and well-documented models in this domain of economic research. It has strong empirical microfoundations and reproduces a large set of empirical stylized facts. Ongoing work focuses on the analysis of policy effects considering spatial factors and knowledge- and information flows. In particular we are interested in studying the effects of:

\begin{itemize}
\item the application of different industrial policy measures in different regions,
\item the existence of varying spatial frictions on goods and labour markets,
\item the spatial dynamics of industrial activity, technical change and growth,
\item micro- and macroprudential regulations and their effects on micro-fragility and macro-financial stability,
\item financialization of the real sector and the need for productive credit for economic development.
\end{itemize}

The Eurace@Unibi model is adapted and extended on a regular basis to suitably address concrete research questions in economic policy. Finally, members of the ETACE group develop and apply statistical methods and concepts to systematically and rigorously analyse computational policy experiments using agent-based simulation models.

The data being generated by such simulation models can be quite complex, not just in terms of data volumes but also in terms of its dimensions, heterogeneity, and variety. This is especially true when large-scale agent-based models with large agent populations are simulated. To analyse such high-dimensional data new data visualization techniques must be developed, and this was one of the main tasks to be accomplished by the ETACE group in the context of the Conquaire project.

Since the Eurace@Unibi model, which was selected as our Use Case for the Conquaire project, has been implemented in the simulation environment FLAME, we give a brief description of this simulation platform below.